<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html>
    <head>
        <meta charset="UTF-8" >
        <link rel="stylesheet" href="css/bootstrap.css">
        <title>ANDIC A.C.</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

    </head>
    <body>
        <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
            <!-- El logotipo y el icono que despliega el menú se agrupan
                 para mostrarlos mejor en los dispositivos móviles -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse"
                        data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Desplegar navegación</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <a href="index.php"> <img src="img/logo1.png" alt="imagen" class="navbar-brand"></a>
            </div>

            <!-- Agrupar los enlaces de navegación, los formularios y cualquier
                 otro elemento que se pueda ocultar al minimizar la barra -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">


                <form class="navbar-form navbar-left" role="search">
                    <div class="form-group">
                        <input type="text" class="form-control" placeholder=" Buscar"><br>
                    </div>
                    <button type="submit" class="btn btn-default"><span class="glyphicon glyphicon-search"></span> Buscar</button>
                </form>
                <ul class="nav navbar-nav navbar-right">
                    <li class="ingresar"><a href="iniciarSesion.php"><u> Ingresar</u></a></li>

                </ul>
            </div>


        </nav>

        <img src="img/portada1.jpg" class="img-responsive" alt="Imagen responsive">



             <div class="masthead">
            <nav>
                <ul class="nav nav-justified">
                    <li class="active"><a href="index.php" data-toogle="tab">Inicio</a></li>
                    <li><a href="conocenos.php">Conócenos</a></li>
                    <li><a href="registro.php">Intégrate</a></li>

                </ul>
            </nav>
        </div>
        
        <p align="center">   
        
        <div class="container-fluid" >
    <div id="main_area" >
      
            <div class="col-xs-8">
                <div  id="slider">
                    <!-- Top part of the slider -->
                    <div class="row">
                        <div id="carousel-bounding-box">
                            <div class="carousel slide" id="myCarousel" >
                                <!-- Carousel items -->
                                <div class="carousel-inner">
                                    <div class="active item" data-slide-number="0">
                                        <img src="img/logo1.png"></div>

                                    <div class="item" data-slide-number="1">
                                        <img src="img/im2.jpg"></div>

                                    <div class="item" data-slide-number="2">
                                        <img src="img/im1.jpg"></div>

                                    <div class="item" data-slide-number="3">
                                        <img src="img/im3.jpg"></div>

                                    <div class="item" data-slide-number="4">
                                        <img src="img/im4.jpg"></div>

                                    <div class="item" data-slide-number="5">
                                        <img src="img/im5.jpg"></div>
                                    
                                    <div class="item" data-slide-number="6">
                                        <img src="img/im6.jpg"></div>
                                    
                                    
                                </div>
                                <!-- Carousel nav -->
                                <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                                    <span class="glyphicon glyphicon-chevron-left"></span>
                                </a>
                                <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                                    <span class="glyphicon glyphicon-chevron-right"></span>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        
        
            <!--/Slider-->
        </div>
            
        </div>
   </p> 
  <!-- Slider -->
        <div class="row">
            
                <!-- Bottom switcher of slider -->
                <ul class="hide-bullets">
                    <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-0">
                            <img src="img/logo1.png">
                        </a>
                    </li>

                    <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-1"><img src="img/im2.jpg"></a>
                    </li>

                    <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-2"><img src="img/im1.jpg"></a>
                    </li>

                    <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-3"><img src="img/im3.jpg"></a>
                    </li>

                     <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-4"><img src="img/im4.jpg"></a>
                    </li>

                      <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-5"><img src="img/im5.jpg"></a>
                    </li>
                    <li class="col-md-2">
                        <a class="thumbnail" id="carousel-selector-6"><img src="img/im6.jpg"></a>
                    </li>

                    
                </ul>
            </div>
   
    
    
        
     
      <footer class="footer-basic-centered">

			<p class="footer-company-motto" align="center">Asociación Nacional para el Desarrollo Integral Comunitario A.C.</p>
                        <br>
                        <div class="row">
  <div class="col-xs-6 col-md-4" align="center"> <a href="https://www.facebook.com/ANDICAC"><img src="img/face.png" id="thumbnail1"></a>  .   .   .
                            
                            <a href=""><img src="img/whats.jpg" id="thumbnail1"></a></div>
                            <div class="col-xs-6 col-md-4" id="n3">Tel. (0155) 5531149389 <br>
                            Francisco Andrade Mz. 24 Lt. 10
                             Santa Catarina Yecahuizotl, Tláhuac Ciudad de México.</div>
                            <div class="col-xs-6 col-md-4">  
                                <a href="https://www.google.com/maps/place/Francisco+Andrade+7,+Santiago,+13300+Ciudad+de+M%C3%A9xico,+CDMX,+M%C3%A9xico/@19.3136113,-98.9678618,17z/data=!4m5!3m4!1s0x85ce1da64a0b64a9:0x721ea511e2472f26!8m2!3d19.3136113!4d-98.9656731?hl=es-419">
                                    <img src="img/mapa.jpg" id="thumbnail2"></a></div>
</div>
                        <br>
			

			<p class="footer-company-name" align="center">Company Name &copy; 2017</p>

		</footer>
        
        
        <script src="js/bootstrap.min.js"></script>
        <script class="cssdeck" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.8.0/jquery.min.js"></script>

        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
 
    </body>

</html>





